\section{Theory}\label{sec:theory}

Let us start from the case of monochromatic incident x-ray beam.
According to the Fermi golden rule the partial RPE cross section 
summed over the final vibrational states of the ion  
is given (in a.u.) by
\begin{eqnarray}
\sigma^0_j(\omega,E)\>=\>\sum\limits_{m_j}
\sigma_{j m_j}(\omega)\delta(\omega+{\cal E}_0
-E-{\cal E}_{j,m_j}), \nonumber\\
\label{eq:cross}
\\
\sigma_{j m_j}(\omega)\>=\>4\pi^2 \alpha \omega
\>\sum\limits_{\beta}
\mid\langle\Psi_0\mid {\bf e}\cdot {\bf D} \mid
\Psi_{j,m_j,\beta,E}\rangle\mid^2, 
\nonumber
\end{eqnarray}
where $\alpha=1/137$ is the fine-structure constant,  
$\omega$ and ${\bf e}$ are  the x-ray photon energy and polarization vector;
${\bf D}$ is the dipole operator.
The final lifetime broadening $\Gamma_f$  is usually smaller than the lifetime
width $\Gamma$ of core excited state. Due to this $\Gamma_f$ is neglected
here.
The final state with  degeneracy label $\beta$,
with ingoing wave boundary conditions and normalized to unit energy 
interval, 
$\mid \Psi_{j,m_j,\beta,E}>$,
corresponds to the emission of a photoelectron of
energy $E$ leaving the ion in the electronic state $j$ and
vibrational state $m_j$.
The index $j$ identifies the ion electronic
wave function $\mid \Phi^{N-1}_j\rangle$ while the index $\beta$ represents
the symmetry of the continuum one-electron wave function $\phi_{\beta E}$ 
of the photoelectron (for example $\phi_{\sigma E}$, 
$\phi_{\pi_x E}$, $\phi_{\pi_y E}$ etc).
The label $m_j$ must be here considered an "asymptotic" label
because, as will be shown in the following, 
the total wave function of the final state cannot be simply 
represented as the product of an electronic and a vibronic 
wave function owing to the resonant contribution $\mid \Phi_1>\mid m>$ 
which depends on the vibrational wave function $\mid m>$ of the 
intermediate electronic state  $\mid \Phi_1>$.
The total energies ${\cal E}_0=E_0+
\epsilon_0$ and ${\cal E}_{j,m_j}=E_j+\epsilon_{m_j}$,
of ground and ionized final states, respectively,
consist of the total electronic
energies ($E_0$ and $E_j$) and the corresponding vibrational energies
$\epsilon_0$ and $\epsilon_{m_j}$.


The RPE cross section for the arbitrary
spectral distribution, $\Phi(\Omega,\gamma)$, of incoming radiation is 
the following convolution \cite{farisINT,farisREV}
\beq\label{eq:conv}
\sigma_j(\omega,E)\>=\>\int d\omega_1\;
\sigma^0_j(\omega,E)\Phi(\omega-\omega_1,\gamma).
\eeq
Here $\gamma$ is the spectral width (HWHM) of $\Phi(\Omega,\gamma)$.



\subsection{Solution of the Fano problem for electron-vibronic states}
\label{sec:fano}

The basic problem in RPE is the evaluation of the final state wave
function.
We consider here the case of several discrete states embeded in
several continuum states with the constraint that the
discrete states consist of one final electronic state $\mid\Phi_1\rangle$
with the corresponding complete set of vibrational levels $\mid m\rangle $.
Making use the solution of this Fano problem \cite{carravetta97} one can write
\beq\label{eq:wave1}
\mid\Psi_{jm_j\beta E}\rangle\>=\>\mid\Phi_{j\beta E}\rangle\mid m_j\rangle+
\sum_{m}\frac{V_{j\beta E}}{E-\omega_{mm_j}-\imath\Gamma} 
\mid\tilde{\Phi}_1\rangle
\langle m\mid m_j\rangle\mid m\rangle,
\eeq
which represents the mixing of the vibrational states
$\mid m\rangle$ of the discrete state $\mid\Phi_1\rangle$
with the vibrational states $\mid m_j\rangle $ of the final electronic 
continua  $\mid\Phi_{j\beta E}\rangle$,
due to the interchannel electronic interaction between the electronic discrete 
state $\mid \Phi_1\rangle$ and the electronic continuum states 
$\mid \Phi_{j\beta E}\rangle$ 
\beq\label{eq:interchan}
V_{j\beta E}\>=\>\langle\Phi_1\mid H_e \mid \Phi_{j\beta E}\rangle.
\eeq
Here $H_e$ is the electronic hamiltonian, 
$\Gamma=\pi\sum_{j\beta}|V_{j\beta E}|^2$,
$\omega_{mm_j}={\cal E}_{1m}-{\cal E}_{jm_j}$,  
${\cal E}_{1m}=E_1+\epsilon_m$, and
$E_{1}$ is the energy of the Fano resonance. Both $\mid m\rangle$ and
$\mid m_j\rangle $ can be bound as well dissociative nuclear states.
The interaction (\ref{eq:interchan}) resulting in an autoionization
modifies the discret state
\beq\label{eq:discret}
\mid \tilde{\Phi}_1\rangle\>=\>\mid \Phi_1\rangle+
\sum_{j^{\prime}\beta^{\prime}}\Big[\wp \int dE^{\prime}
\frac{V_{j^{\prime}\beta^{\prime}E^{\prime}}}{E-E^{\prime}} 
|\Phi_{j^{\prime}\beta^{\prime}E^{\prime}}\rangle
+\imath\pi V_{j^{\prime}\beta^{\prime}E}
\mid\Phi_{j^{\prime}\beta^{\prime}E}\rangle\Big].
\eeq
The electronic wave functions $\Phi_0$, $\Phi_1$ and  $\Phi_{j\beta E}$
are chosen here to be real.
Now we are in stage to write the expression for the RPE amplitude \\
($F_{jm_j\beta E}=\langle \Psi_{jm_j\beta E}
|{\bf e}\cdot{\bf D}|\Psi_0\rangle$), making use the energy conservation 
law (\ref{eq:cross}),
\beq\label{eq:amp}
F_{jm_j\beta E}\>=\>
\Big({\bf e}\cdot{\bf D}_{j\beta E}\Big)\langle m_j|0\rangle+
\sum\limits_m\frac{V_{j\beta E}\Big({\bf e}\cdot{\bf D}_{1 E}\Big)}
{\omega-\omega_{1m,0}+\imath\Gamma}
\langle m_j|m\rangle\langle m|0\rangle,
\eeq
$\omega_{1m,0}={\cal E}_{1m}-{\cal E}_0$.
The dipole moments of the direct and resonant contributions are,
respectively,
\beq\label{eq:dipole}
{\bf D}_{j\beta E}\>=\>\langle \Phi_{j\beta E}|{\bf D}|\Phi_0\rangle,
\;\;\;\;\;
{\bf D}_{1 E}\>=\>\langle \tilde{\Phi}_{1 E}|{\bf D}|\Phi_0\rangle.
\eeq
The Born-Oppenheimer (BO) approximation for the ground state
was used here, according to which the molecular wave function is a product
of electronic and nuclear wave functions:
$|\Psi_0\rangle=|\Phi_0\rangle|0\rangle$.


The RPE cross section of gas phase molecules integrated over the angular
distribution of photoelectrons  can be received by the following formal 
replacement ($e_ie_j\rightarrow \delta_{ij}/3$) or what is the same:
\beq\label{eq:replace}
{\bf e}\>\rightarrow \>\frac{1}{\sqrt{3}}.
\eeq



\subsection{Time-dependent representation for the RPE cross section}
\label{sec:time}

We follow to the time-dependent wave packet technique in numerical simulations.
Therefore, it is appropriate to write the cross section (\ref{eq:cross})
with the scattering amplitude(\ref{eq:amp}) in the time-dependent
representation \cite{salek98,farisREV}
\beq\label{eq:timecross}
\sigma^0_j(\omega,E)\>=\>4\pi\alpha\omega\int\limits_0^\infty
d\tau\;\sigma^0_j(\tau)e^{\imath(\omega-E+{\cal E}_0)\tau}.
\eeq
It is assumed here for a simplicity that a light is linear polarized
and electronic transition matrix elements
do not depend on the nuclear coordinates; this dependence is taken into
account elsewere \cite{salekGFC}.
The autocorrelation function 
\beq\label{eq:sum}
\sigma^0_j(\tau)\>=\>\sum\limits_\beta\Big[
|{\bf e}\cdot{\bf D}_{j\beta E}|^2\sigma^{d}_j(\tau)+
|V_{j\beta E}({\bf e}\cdot{\bf D}_{1E})|^2\sigma^{r}_j(\tau)+
V_{j\beta E}({\bf e}\cdot{\bf D}_{1 E})
({\bf e}\cdot{\bf D}_{j\beta E})\sigma^{int}_j(\tau)\Big]
\eeq
is  the sum of direct, resonant and
interference contributions, respectively,
\begin{eqnarray}\label{eq:drint}
\sigma^{d}_j(\tau)\>&=&\>
\langle \psi^d_{j}(0)|\psi^d_{j}(\tau)\rangle,\;\;\;\;\;\;
\sigma^{r}_j(\tau)\>=\>
\langle \psi^r_{j}(0)|\psi^r_{j}(\tau)\rangle,\nonumber\\
\\
\sigma^{int}_j(\tau)\>&=&\>\imath
\Big[\langle \psi^r_{j}(0)|\psi^d_{j}(\tau)\rangle e^{-2\imath \varphi(E)}-
\langle \psi^d_{j}(0)|\psi^r_{j}(\tau)\rangle\Big].
\nonumber
\end{eqnarray}
The wave packet induced by the direct photoemission
\beq\label{eq:dirwp}
|\psi^d_{j}(\tau)\rangle\>=\>e^{-\imath h_f\tau} |0\rangle
\eeq
describes the nuclear dynamics in the final state potential surface with
the final state nuclear hamiltonian $h_f$ and initial condition
$|\psi^d_{j}(0)\rangle=|0\rangle$. The resonant photoemission
leads to the wave packet
\beq\label{eq:reswp}
|\psi^r_{j}(\tau)\rangle\>=\>e^{-\imath h_f\tau} 
|\psi^r_{j }(0)\rangle,\;\;\;\;\;\;
|\psi^r_{j}(0)\rangle\>=\>\int\limits_0^\infty dt\;
e^{[\imath(\omega+{\cal E}_0)-\Gamma ]t} |\psi(t)\rangle.
\eeq
The evolution of the wave packet 
$\psi(t)=\exp(-\imath h_it)|0\rangle$ follows to the 
Schr{\"o}dinger equation with the nuclear hamiltonian $h_i=K+U_i$ of core
excited state and the initial condition 
$\psi(0)=|0\rangle$. Here $K$ is the nuclear kinetic 
energy operator and $U_i$ is the total nuclear potential energy of
the i $th$  electronic state.
The wave function of autoionizing state $\Phi_{1 E}$ (\ref{eq:discret}) 
is complex in the general case. This results in the phase  
$\varphi(E)=\arg({\bf e}\cdot{\bf D}_{1E})$.

When detector collects electrons from all directions we have to use
the following autocorrealtion function
\beq\label{eq:sum1}
\sigma^0_j(\tau)\>=\>\frac{1}{3}\sum\limits_\beta\Big[
|{\bf D}_{j\beta E}|^2\sigma^{d}_j(\tau)+
V^2_{j\beta E}|{\bf D}_{1E}|^2\sigma^{r}_j(\tau)+
V_{j\beta E}({\bf D}_{1 E}\cdot
{\bf D}_{j\beta E})\sigma^{int}_{j\beta }(\tau)\Big],
\eeq
where $\sigma^{int}_{j\beta }(\tau)$ is given by eq.(\ref{eq:drint})
with $\varphi(E)\rightarrow \varphi_{j\beta}(E)=
\arg({\bf D}_{j\beta E}\cdot{\bf D}_{1E})$.



\section{The RPE spectra of HF: Model}\label{sec:model}

$\bullet$ {\bf Vincenzo}, We use serious assumption:
We assume that the wave function $\tilde{\Phi}_1$ 
is real (the last term at the right hand side of eq.(\ref{eq:discret}) is 
neglected). Due to this the transition matrix element 
${\bf D}_{1E}$ (\ref{eq:dipole}) is also real !!!

Let us consider the spectator RPE spectra of the gas phase HF molecules.
The RPE cross section is integrated over injection directions
of photoelectrons. We choose the molecular axis along the z axis.

Direct channel are: $1\pi\rightarrow \Pi$, $1\pi\rightarrow \Sigma,\Delta$.

According to symmetry the following transition dipole moments
for direct transions, ${\bf D}_{j\beta E}$, are not equal to zero:
\begin{eqnarray}\label{eq:dd}
x^d\>&=&y^d\>=\>{\bf D}^x_{j\beta E}\>=\>
\langle \Phi_0|{\bf D}^z|1\pi_x\rightarrow\Pi_x\rangle,
\nonumber\\
\\
z^d\>&=&\>{\bf D}^z_{j\beta E}\>=
\>\langle \Phi_0|{\bf D}^z|1\pi\rightarrow\Sigma,\Delta\rangle,\nonumber
\end{eqnarray}
Only one dipole transition matrix element differs from zero
for resonant channel ($1s\rightarrow \sigma^*$):
\beq\label{eq:dr}
z^r\>=\>{\bf D}^z_{1 E}\>=\>
\langle \Phi_0|{\bf D}^z|1s\rightarrow\sigma^*\rangle.
\eeq
We have only two nonzero 
decay amplitude for the decay transition $1\pi\rightarrow\Pi$:
\beq\label{eq:decay}
q\>=\>\langle \Phi_0|V|1\pi_x\rightarrow\Pi_x\rangle\>=\>
\langle \Phi_0|V|1\pi_y\rightarrow\Pi_y\rangle.
\eeq



So we can rewrite eq.(\ref{eq:sum1}) for the considered model as the following:
\beq\label{eq:sumHF}
\sigma^0_j(\tau)\>=\>\frac{1}{3}\Big[
\{2(x^d)^2+(z^d)^2\}\sigma^{d}(\tau)+
2 (z^r q)^2\sigma^{r}(\tau)+
2  z^d z^r q
\sigma^{int}(\tau)\Big], \;\;\;\;\;\varphi_{j\beta}(E)\>=\>0.
\eeq
The autocorrelation functions $\sigma^{d}(\tau)$, $\sigma^{r}(\tau)$, and
$\sigma^{int}(\tau)$ are defined by eq.(\ref{eq:drint}), 
where index $j$ is ommited.







\section{Summary}\label{sec:disc}




\vskip 1.5truecm   
\begin{center}
   {\bf ACKNOWLEDGEMENTS}
   \end{center}
This work was supported by the Swedish National Research council (NFR),
the Wenner-Gren Foundation, and the Swedish Institute (SI).
\vskip 1.5truecm   
   \noindent
   $^1$ Permanent address:
   Institute of Automation and Electrometry, 630090
   Novosibirsk, Russia
   \vskip 1.7truecm


\bibliography{emissb}
\vskip 0.3truecm
\noindent
\bibliographystyle{unsrt}

%\newpage

%\centerline{\bf Figure Captions}

%Fig.1.
\vskip 3 pt
\begin{figure}
\caption{The  C 1s$^{-1}\pi^*$ RPE
}
\label{fig:spectrum}
\end{figure}
% 
%Fig.2.
\vskip 3 pt
\begin{figure}
\caption{The squared wave packet 
}
\label{fig:wavepacket}
\end{figure}
%
%Fig.3.
\vskip 3 pt
\begin{figure}
\caption{The  electronic Doppler shift 
}
\label{fig:doppler}
\end{figure}
% 


\end{document}


















---310195404-1957747793-963478507=:2377--

